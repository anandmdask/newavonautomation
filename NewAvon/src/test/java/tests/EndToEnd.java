package tests;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import framework.utils.APIClient;
import framework.utils.APIException;
import framework.utils.ReadExcel;
import framework.wrappers.NewAvonWrapper;
import pages.BrouchresPage;
import pages.CheckoutPage;
import pages.HomePage;
import pages.LoginPage;
import pages.OpenOrdersPage;
import pages.OrderConfirmationPage;
import pages.ProductDetailsPage;
import pages.ProductListPage;


public class EndToEnd extends NewAvonWrapper {
	
	public String TEST_RUN_ID  = "1594";
    
 
	
	@BeforeClass
	@Parameters({"Automation","Browser","Market","PlatformName","PlatformVersion","DeviceName","udid","AppiumVersion","DeviceOrientation","Application","Port","TestRailIntegration"})
	public void setData(String automation,String browser, String mrkt,String platformNam, String platformVer, String deviceNam, String udid, String appiumVer, String devOrientation,String application,String port,String testRailIntegration) {
		
		testDescription="Login Test Case";
		category="Functional";
		
		browserName=browser;
		market=mrkt;
		applicationType=application;
		authors="Infosys QA Team";
		
		capData.put("Automation", automation);
		capData.put("Browser", browser);
		capData.put("Market", mrkt);
		capData.put("Port", port);
		capData.put("PlatformName", platformNam);
		capData.put("PlatformVersion", platformVer);
		capData.put("DeviceName", deviceNam);
		capData.put("udid", udid);
		capData.put("AppiumVersion", appiumVer);
		capData.put("DeviceOrientation", devOrientation);
		capData.put("TestCaseName", testCaseName);
		capData.put("testRailIntegration", testRailIntegration);
		ReadExcel readExcel=new ReadExcel();
		
		
		try {
			readExcel.openExcel();
			} catch (IOException e) {
			
			e.printStackTrace();
			}

	}
	
	

  @Test(description="Login -> PLP -> PDP -> Cart -> Checkout -> Order Confirmation", enabled=false)
  public void productDetails() throws IOException, APIException {
	  
	  testCaseName="Successful Login";
	  startExtentReport(market, applicationType, browserName);
	  ReadExcel readExcel=new ReadExcel();
	  readExcel.excelRead("ProductDetailsE2E",capData);
	  new LoginPage(driver, test, market, capData)
	  .login(readExcel);
	  
	  new HomePage(driver, test, market, capData)
	  .navigateToProducts(readExcel);
	  
	  new ProductListPage(driver, test, market, capData)
	  .navigateToPDPRandomSelect();
	  
	 String lineNum= new ProductDetailsPage(driver, test, market, capData)
			 			.addToOrder(readExcel);
	 new ProductDetailsPage(driver, test, market, capData)
	 .VerifyCartItems();
	 
	 new OpenOrdersPage(driver, test, market, capData)
	 .orderCheckout(readExcel, lineNum);
	 
	 new CheckoutPage(driver, test, market, capData)
	 .orderSubmit(readExcel);
	 
	 new OrderConfirmationPage(driver, test, market, capData)
	 .confirmOrderSuccess();
	 
	  if(capData.get("testRailIntegration").equalsIgnoreCase("YES"))
		  if(test.getRunStatus().toString().equalsIgnoreCase("PASS"))
			  addResultForTestCase(TEST_RUN_ID,"47215", TEST_CASE_PASSED_STATUS, "");
		  else
			  addResultForTestCase(TEST_RUN_ID,"47215", TEST_CASE_FAILED_STATUS, "");
  }
  
  @Test(description="Login -> Brochures -> Quick Shop Modal -> Cart -> Checkout -> Order Confirmation", enabled=true)
  public void brouchreValidation() throws IOException, APIException
  {
	  testCaseName="Brouchre E2E Validation";
	  startExtentReport(market, applicationType, browserName);
	  ReadExcel readExcel=new ReadExcel();
	  try {
		readExcel.excelRead("BrouchreE2E",capData);
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	  new LoginPage(driver, test, market, capData)
	  .login(readExcel);
	  
	  new HomePage(driver, test, market, capData)
	  .navigateToBrouchres();
	  
	  new BrouchresPage(driver, test, market, capData)
	  .selectCampgain(readExcel);
	  
	  new BrouchresPage(driver, test, market, capData)
	  .selectRandomProduct(readExcel);
	  
	 String lineNum= new BrouchresPage(driver, test, market, capData)
	  .quickShop(readExcel);
	  
	 new ProductDetailsPage(driver, test, market, capData)
	 .VerifyCartItems();
	 
	 new OpenOrdersPage(driver, test, market, capData)
	 .orderCheckout(readExcel, lineNum);
	 
	 new CheckoutPage(driver, test, market, capData)
	 .orderSubmit(readExcel);
	 
	 new OrderConfirmationPage(driver, test, market, capData)
	 .confirmOrderSuccess();
	 
	  if(capData.get("testRailIntegration").equalsIgnoreCase("YES"))
		  if(test.getRunStatus().toString().equalsIgnoreCase("PASS"))
			  addResultForTestCase(TEST_RUN_ID,"47217", TEST_CASE_PASSED_STATUS, "");
		  else
			  addResultForTestCase(TEST_RUN_ID,"47217", TEST_CASE_FAILED_STATUS, "");
	  
  }
  
}
