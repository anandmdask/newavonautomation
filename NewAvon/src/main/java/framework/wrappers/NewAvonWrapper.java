package framework.wrappers;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import com.relevantcodes.extentreports.LogStatus;

import framework.utils.APIClient;
import framework.utils.APIException;
import framework.utils.Reporter;

public class NewAvonWrapper extends GenericWrappers {
	
	public String browserName;
	public String dataSheetName;
	public String market;
	public String applicationType;
	
	public HashMap<String, String> reportUrl=new HashMap();
	
	public String TESTRAIL_USERNAME          = "anand.mohandas@infosys.com";
    public String TESTRAIL_PASSWORD          = "Anan92$$";
    public String RAILS_ENGINE_URL           = "https://newavonllc.testrail.net/";
	
    public final int TEST_CASE_PASSED_STATUS = 1;
    public final int TEST_CASE_FAILED_STATUS = 5;
		
	@BeforeSuite(alwaysRun=true)
	public void beforeSuite() {
		startResult();
		
		
	}
	
	
	@BeforeTest
	public void beforeTest(){
		
		
	}
	

	@BeforeMethod(alwaysRun=true)
	public void beforeMethod(){

	}
		
	@AfterSuite(alwaysRun=true)
	public void afterSuite() {
		endResult();
		
	}

	@AfterTest(alwaysRun=true)
	public void afterTest(){
		

	
	}
	
	@AfterMethod(alwaysRun=true)
	public void afterMethod() throws IOException{

		
		endTestcase();
		quitBrowser();
		
		
		
	}
	
	
//	@DataProvider(name="fetchData")
//	public Object[][] getData(){
//			//System.out.println(market);
//		return DataInputProvider.getSheet(market,dataSheetName,applicationType);		
//		
//	}	
	
	@AfterMethod(alwaysRun=true)
	@Parameters({"Automation","Browser","Market","PlatformName","PlatformVersion","DeviceName","udid","AppiumVersion","DeviceOrientation","Application","Port","TestRailIntegration"})
	public void setDataAgain(String automation,String browser, String mrkt,String platformNam, String platformVer, String deviceNam, String udid, String appiumVer, String devOrientation,String application,String port,String testRailIntegration) throws FileNotFoundException, IOException
	{
		capData.remove(capData);
		capData.put("Automation", automation);
		capData.put("Browser", browser);
		capData.put("Market", mrkt);
		capData.put("Port", port);
		capData.put("PlatformName", platformNam);
		capData.put("PlatformVersion", platformVer);
		capData.put("DeviceName", deviceNam);
		capData.put("udid", udid);
		capData.put("AppiumVersion", appiumVer);
		capData.put("DeviceOrientation", devOrientation);
		capData.put("TestCaseName", testCaseName);
		capData.put("testRailIntegration", testRailIntegration);
	}
	
	public void addResultForTestCase(String TEST_RUN_ID,String testCaseId, int status,
	          String error) throws IOException, APIException {

	      String testRunId = TEST_RUN_ID;
	      APIClient client = new APIClient(RAILS_ENGINE_URL);
	      client.setUser(TESTRAIL_USERNAME);
	      client.setPassword(TESTRAIL_PASSWORD);
	      Map data = new HashMap();
	      data.put("status_id", status);
	      data.put("comment", "Test Executed - Status updated automatically from Selenium test automation.");
	      client.sendPost("add_result_for_case/"+testRunId+"/"+testCaseId+"",data );

	  }
	
}






