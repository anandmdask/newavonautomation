package framework.utils;

public class APIException extends Exception
{
	public APIException(String message)
	{
		super(message);
		System.out.println(message);
	}
}