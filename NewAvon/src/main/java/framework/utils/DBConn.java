package framework.utils;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;

import framework.wrappers.NewAvonWrapper;
import framework.wrappers.Wrappers;

public class DBConn {

	String JDBC_DRIVER;
	String DB_URL;
	String USER;
	String PASS;
	Connection conn=null;
	Statement stmt=null;
	ResultSet rs=null;
	
	String market;
	String marketid;

	public DBConn(String market){
		this.market=market;
	}
	public  void setDBDetails(){

		//DB Details
	}

	public  void connectToDB() throws ClassNotFoundException, SQLException{
		setDBDetails();
		//STEP 2: Register JDBC driver
		Class.forName("oracle.jdbc.driver.OracleDriver");
		//STEP 3: Open a connection
		System.out.println("Connecting to database..."+ market);
		conn = DriverManager.getConnection(DB_URL,USER,PASS);
		conn.setAutoCommit(false);
		System.out.println("connected successfully");
	}

	public ResultSet runQuery(String sqlQuery) throws SQLException{
		//STEP 4: Execute a query
		stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(sqlQuery);
		System.out.println(rs);
		return rs;
	}



	public void closeConnection() throws SQLException{
		conn.close();
		System.out.println("Connection Closed");
	}



	
    public String getDbDetails(String query,List<String> cond){
    	int i=1;
    	String temp;
    	ResultSet rs;
    	String result = null;
    	try {
    		connectToDB();
    		PreparedStatement ps=conn.prepareStatement(query);
    		while(i<=cond.size())
    		{
    			temp=cond.get(i-1);
    			ps.setString(i, temp);
    			i++;
    		}
    		rs=ps.executeQuery();


    		while(rs.next()){
    			result=rs.getString(1);
    		}

    		
    		rs.close();
    		closeConnection();
    	} catch (ClassNotFoundException | SQLException e) {
    		
    		e.printStackTrace();
    	}
    	return result;
    }
    public List getMultippleDbColumnDetails(String query){
    	ResultSet rs;
    	List<String> resultList=new ArrayList<String>();
    	try {
    		connectToDB();
    		rs=runQuery(query);
    		int colCount=1;
    		ResultSetMetaData rsmd=rs.getMetaData();
    		int columnCount=rsmd.getColumnCount();
    		while(rs.next()){
    			while(colCount<=columnCount)
    				resultList.add(rs.getString(colCount++));
    		}
    		stmt.close();
    		rs.close();
    		closeConnection();
    	} catch (ClassNotFoundException | SQLException e) {
    		
    		e.printStackTrace();
    	}
    	return resultList;
    }
    
    
    public List<String> getMultippleDbColumnDetails(String query, List<String> cond){
    	ResultSet rs;
    	List<String> resultList=new ArrayList<String>();
    	try {
    		connectToDB();
    		PreparedStatement ps=conn.prepareStatement(query);
    		int i=1;
    		String temp;
    		while(i<=cond.size())
    		{
    			temp=cond.get(i-1);
    			ps.setString(i, temp);
    			i++;
    		}
    		rs=ps.executeQuery();
    		int colCount=1;
    		ResultSetMetaData rsmd=rs.getMetaData();
    		int columnCount=rsmd.getColumnCount();
    		while(rs.next()){
    			while(colCount<=columnCount)
    				resultList.add(rs.getString(colCount++));
    		}
    		ps.close();
    		rs.close();
    		closeConnection();
    	} catch (ClassNotFoundException | SQLException e) {
    		
    		e.printStackTrace();
    	}
    	return resultList;
    	}
    public Map<Integer,List<String>> getMultipleDbRowDetails(String query, List<String> cond){
    	ResultSet rs;
    	Map<Integer,List<String>> rowData= new TreeMap<Integer,List<String>>();
    	int key=1;
    	try {
    		connectToDB();
    		PreparedStatement ps=conn.prepareStatement(query);
    		int i=1;
    		String temp;
    		while(i<=cond.size())
    		{
    			temp=cond.get(i-1);
    			ps.setString(i, temp);
    			i++;
    		}
    		rs=ps.executeQuery();
    		int colCount=1;
    		ResultSetMetaData rsmd=rs.getMetaData();
    		int columnCount=rsmd.getColumnCount();
    		while(rs.next()){
                    List<String> resultList=new ArrayList<String>();
                    colCount=1;
                    while(colCount<=columnCount) {
                                    resultList.add(rs.getString(colCount++));
                    }
                    rowData.put(key,resultList);
                    key++;
    		}
    		ps.close();
    		rs.close();
    		closeConnection();
    	} catch (ClassNotFoundException | SQLException e) {
    		
    		e.printStackTrace();
    	}
    	return rowData;
    	}
    
    
   

    
 public void updateTable(String query,List<String> cond)
 {
	 	try {
			connectToDB();
		
			PreparedStatement ps=conn.prepareStatement(query);
			int i=1;
			String temp;
		
			while(i<=cond.size())
			{
				temp=cond.get(i-1);
				
				ps.setString(i, temp);
				i++;
			}
			ps.executeUpdate();
			conn.commit();
	 	} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
 }
 public void insertTable(String query,List<String> cond)
 {
	 	try {
			connectToDB();
		
			PreparedStatement ps=conn.prepareStatement(query);
			int i=1;
			String temp;
		
			while(i<=cond.size())
			{
				temp=cond.get(i-1);
				System.out.println(temp);
				ps.setString(i, temp);
				i++;
			}
			ps.executeUpdate();
			conn.commit();
			System.out.println("Insert Successful");
	 	} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
 }
 
 
 
}