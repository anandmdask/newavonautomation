package framework.utils;


import java.io.FileInputStream;

import java.io.FileNotFoundException;

import java.io.IOException;

import java.util.Base64;

import java.util.HashMap;
import java.util.Map;

import org.apache.poi.ss.usermodel.Row;

import org.apache.poi.ss.usermodel.Sheet;

import org.apache.poi.ss.usermodel.Workbook;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;


import framework.wrappers.GenericWrappers;


public class ReadExcel {


	HashMap<String, String> excelValue;

	Workbook wb;

	FileInputStream fs;



	public void openExcel() throws IOException

	{

		String testData=System.getProperty("user.dir");

		fs=new FileInputStream(testData+"//data//DataSheet.xlsx");

		wb=new XSSFWorkbook(fs);

	}


	public void excelRead(String testCaseName,Map<String, String> capData1) throws IOException {

		
		openExcel();
		String sheeetName="generic";
		Sheet sh=wb.getSheet(sheeetName);
		String testSheet="";
		Sheet sprintSh;
		excelValue=new HashMap();

		int lastRow=sh.getLastRowNum();

		int rowNum;

		Row row = null;

		

		for(rowNum =1;rowNum<=lastRow;rowNum++)

		{

			row=sh.getRow(rowNum);

			if(row.getCell(0).getStringCellValue().equalsIgnoreCase(capData1.get("Market").toLowerCase()+"."+testCaseName))
			{
				testSheet=row.getCell(1).getStringCellValue();
				break;
			}

		}

		sprintSh=wb.getSheet(testSheet);
		
		Row row0=sprintSh.getRow(0);
		lastRow=sprintSh.getLastRowNum();
		for(rowNum =1;rowNum<=lastRow;rowNum++)

		{
			row=sprintSh.getRow(rowNum);

			if(row.getCell(0).getStringCellValue().equalsIgnoreCase(capData1.get("Market").toLowerCase()+"."+testCaseName))
			{
				break;
			}

		}
		for(int i=1;i<row.getLastCellNum();i++)

		{


			if(row.getCell(i).getCellType()==1)

				excelValue.put(row0.getCell(i).getStringCellValue(), row.getCell(i).getStringCellValue());

			else if(row.getCell(i).getCellType()==0)

				excelValue.put(row0.getCell(i).getStringCellValue(), row.getCell(i).getNumericCellValue()+"");

			else if(row.getCell(i).getCellType()==3)

				continue;

		}


	}


	public String getValue(String key)

	{

		return excelValue.get(key);

	}


	public void closeExcel() throws IOException

	{

		wb.close();

		fs.close();

	}


}

