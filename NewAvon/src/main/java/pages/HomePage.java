package pages;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;

import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.ExtentTest;

import framework.utils.DBConn;
import framework.utils.ReadExcel;
import framework.wrappers.NewAvonWrapper;

public class HomePage extends NewAvonWrapper{
	
	private  Properties prop;
	public Map<String,String> capData = new HashMap<String,String>();
	DBConn db;
	
	public HomePage(RemoteWebDriver driver, ExtentTest test, String mrkt, Map<String,String> capData)
	{
		this.driver = driver;
		this.test = test;
		this.capData=capData;
		this.market=mrkt;
		prop = new Properties();
		
		try {
			prop.load(new FileInputStream(new File("./src/main/Resources/angular/New Avon/Home.properties")));
			db= new DBConn(mrkt);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void navigateToProducts(ReadExcel readExcel)
	{
//		WebDriverWait wait=new WebDriverWait(driver, 100);
//		wait.until(ExpectedConditions.elementToBeClickable(getLocator(prop.getProperty("yourAvonTitle"))));
		waitUntilElementClickable(prop.getProperty("yourAvonTitle"), 100);
		try {
			Thread.sleep(7000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Actions action=new Actions(driver);
		
		verifyStep("Home page is Displayed", "PASS");
		StringTokenizer st=new StringTokenizer(readExcel.getValue("ProductNavigation"),">");
		String category=st.nextToken();
		if(capData.get("Automation").equalsIgnoreCase("selenium"))
		{
			if(!st.hasMoreTokens())
			{
				for(int i=1;i<=getAllLinks("xpath===//ul[@class='nav nav-tabs navbar-main__list m-0']/li").size();i++)
				{
					if(getText("xpath===//ul[@class='nav nav-tabs navbar-main__list m-0']/li["+i+"]/a[1]").trim()
							.equalsIgnoreCase(category))
						click("xpath===//ul[@class='nav nav-tabs navbar-main__list m-0']/li["+i+"]/a[1]");
				}
			}
			else
			{
				int i=1;
				for(i=1;i<=getAllLinks("xpath===//ul[@class='nav nav-tabs navbar-main__list m-0']/li").size();i++)
				{
					if(getText("xpath===//ul[@class='nav nav-tabs navbar-main__list m-0']/li["+i+"]/a[1]").trim()
							.equalsIgnoreCase(category))
					{
						action.moveToElement(driver.findElement
								(getLocator("xpath===//ul[@class='nav nav-tabs navbar-main__list m-0']/li["+i+"]/a[1]"))).build().perform();
						break;
					}
				}
				System.out.println(i);
				String productCat=st.nextToken();
				for(int j=1;j<getAllLinks("xpath===//ul[@class='nav nav-tabs navbar-main__list m-0']/li["+i+"]//ul/li").size();j++)
				{
					if(getText("xpath===//ul[@class='nav nav-tabs navbar-main__list m-0']/li["+i+"]//ul/li["+j+"]/a").trim()
							.equalsIgnoreCase(productCat))
						click("xpath===//ul[@class='nav nav-tabs navbar-main__list m-0']/li["+i+"]//ul/li["+j+"]/a");
				}
				verifyStep("Category Selected", "PASS");
				if(st.hasMoreTokens())
					new ProductListPage(driver, test, market, capData)	
					.navigateToProducts(st.nextToken());
			}
		}
		
		
	}
	
	public void navigateToBrouchres()
	{
//		WebDriverWait wait=new WebDriverWait(driver, 100);
//		wait.until(ExpectedConditions.elementToBeClickable(getLocator(prop.getProperty("yourAvonTitle"))));
		waitUntilElementClickable(prop.getProperty("yourAvonTitle"), 100);
		try {
			Thread.sleep(7000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(capData.get("Automation").equalsIgnoreCase("appium"))
			if(!capData.get("Automation").equalsIgnoreCase(""))
			{
				click(prop.getProperty("hamBurgerMenu"));
			}
		verifyStep("Navigate to Brouchres Page", "PASS");
		click(prop.getProperty("brouchresLnk"));
	}
	
}
