package pages;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.ExtentTest;

import framework.utils.DBConn;
import framework.utils.ReadExcel;

import framework.wrappers.NewAvonWrapper;


public class OrderConfirmationPage extends NewAvonWrapper {
	private  Properties prop;
	public Map<String,String> capData = new HashMap<String,String>();
	DBConn db;
	
	public OrderConfirmationPage(RemoteWebDriver driver, ExtentTest test, String mrkt, Map<String,String> capData)
	{
		this.driver = driver;
		this.test = test;
		this.capData=capData;
		this.market=mrkt;
		prop = new Properties();
		
		try {
			prop.load(new FileInputStream(new File("./src/main/Resources/angular/New Avon/OrderConfirmationPage.properties")));
			db= new DBConn(mrkt);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public OrderConfirmationPage confirmOrderSuccess()
	{
		
		try {
			
			//WebDriverWait wait=new WebDriverWait(driver, 100);
			

			waitUntilElementClickable(prop.getProperty("header"), 100);			
			//wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(getLocator(prop.getProperty("header")))));
			
	
			if(verifyElement(prop.getProperty("header"))){
				verifyStep("Order Submitted Successfully", "PASS");
				
			}
			else
				verifyStep("Order Submission failed ", "FAIL");
			
			String orderNo=getText(prop.getProperty("orderNo"));
			
			verifyStep(orderNo+" is created", "PASS");
		

		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return this;
	}
	

	
}
