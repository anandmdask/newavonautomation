package pages;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.StringTokenizer;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.ExtentTest;

import framework.utils.DBConn;
import framework.utils.ReadExcel;
import framework.wrappers.NewAvonWrapper;

public class ProductListPage extends NewAvonWrapper{
	
	private  Properties prop;
	public Map<String,String> capData = new HashMap<String,String>();
	DBConn db;
	
	public ProductListPage(RemoteWebDriver driver, ExtentTest test, String mrkt, Map<String,String> capData)
	{
		this.driver = driver;
		this.test = test;
		this.capData=capData;
		this.market=mrkt;
		prop = new Properties();
		
		try {
			prop.load(new FileInputStream(new File("./src/main/Resources/angular/New Avon/ProductList.properties")));
			db= new DBConn(mrkt);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void navigateToProducts(String productCategory)
	{
//		WebDriverWait wait=new WebDriverWait(driver, 100);
//		wait.until(ExpectedConditions.elementToBeClickable(getLocator(prop.getProperty("productCategoryList"))));
		waitUntilElementClickable(prop.getProperty("productCategoryList"), 100);		
		StringTokenizer st=new StringTokenizer(productCategory, ">");
		String prod=st.nextToken();
		for(int i=1;i<=getAllLinks(prop.getProperty("productCategoryList")).size();i++)
		{
			if(getText("xpath===//ul[@name='categories']/li["+i+"]/a").trim().equalsIgnoreCase(productCategory))
			{
				click("xpath===//ul[@name='categories']/li["+i+"]/a");
				break;
			}
				
		}
		if(st.hasMoreElements())
		{
			for(int i=1;i<=getAllLinks(prop.getProperty("productSubCategoryList")).size();i++)
			{
				if(getText("xpath===//div[@class='ng-scope collapse in']//li["+i+"]/a").trim().equalsIgnoreCase(st.nextToken().toString()))
				{
					click("xpath===//div[@class='ng-scope collapse in']//li["+i+"]/a");
					break;
				}
					
			}
		}
		verifyStep("Sub Category Selected", "PASS");
	}
	
	public void navigateToPDPRandomSelect()
	{
		Random rand=new Random();
		
		if(capData.get("Automation").equalsIgnoreCase("Selenium"))
		{
			JavascriptExecutor jse = (JavascriptExecutor) driver;
	        jse.executeScript("window.scrollTo(0, document.body.scrollHeight);");
	        jse.executeScript("window.scrollTo(0, document.body.scrollHeight);");
	        jse.executeScript("window.scrollTo(0, document.body.scrollHeight);");
		}
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		getAllLinks(prop.getProperty("productList"))
		.get(rand.nextInt(getAllLinks(prop.getProperty("productList")).size())).click();
	}
}
