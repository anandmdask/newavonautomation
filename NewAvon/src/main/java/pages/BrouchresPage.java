package pages;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.relevantcodes.extentreports.ExtentTest;

import framework.utils.DBConn;
import framework.utils.ReadExcel;
import framework.wrappers.NewAvonWrapper;


public class BrouchresPage extends NewAvonWrapper {
	private  Properties prop;
	public Map<String,String> capData = new HashMap<String,String>();
	DBConn db;
	
	public BrouchresPage(RemoteWebDriver driver, ExtentTest test, String mrkt, Map<String,String> capData)
	{
		this.driver = driver;
		this.test = test;
		this.capData=capData;
		this.market=mrkt;
		prop = new Properties();
		
		try {
			prop.load(new FileInputStream(new File("./src/main/Resources/angular/New Avon/Brouchres.properties")));
			db= new DBConn(mrkt);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public void selectCampgain(ReadExcel readExcel)
	{
		//WebDriverWait wait=new WebDriverWait(driver, 100);
		
		waitUntilElementClickable(prop.getProperty("selectCampgainDrp"), 100);
		//wait.until(ExpectedConditions.elementToBeClickable(getLocator(prop.getProperty("selectCampgainDrp"))));
		
		verifyStep("Campgain Page Verified", "PASS");
		
		click(prop.getProperty("selectCampgainDrp"));
		
		List<WebElement> campgainList=new ArrayList<>();
		campgainList.addAll(getAllLinks(prop.getProperty("campgainSelect")));
		
		for(WebElement campgain:campgainList)
		{
			if(campgain.getText().equalsIgnoreCase(readExcel.getValue("CampgainNumber")))
			{
				campgain.click();
				break;
			}
				
		}
		
		verifyStep("Campgain Number Selected", "PASS");
	}
	
	public void selectRandomProduct(ReadExcel readExcel)
	{
		boolean flag=false;
		
		
		if(!capData.get("Automation").equalsIgnoreCase("appium") && !capData.get("Automation").equalsIgnoreCase("selenium"))
		{
			for(int i=0;i<=20;i++)
			{
				if(getText(prop.getProperty("brouchreNameMobile")).trim().equalsIgnoreCase(readExcel.getValue("BrouchreName")))
				{
					
					click(prop.getProperty("brouchreImgMobile"));
					flag=true;
					break;
				}
				else
				{
					click(prop.getProperty("nextCarousel"));
				}
			}
		}
		else
		{
			for(int i=0;i<=20;i++)
			{
				if(getText(prop.getProperty("brouchreName")).trim().equalsIgnoreCase(readExcel.getValue("BrouchreName")))
				{
					
					click(prop.getProperty("brouchreImg"));
					flag=true;
					break;
				}
				else
				{
					click(prop.getProperty("nextCarousel"));
				}
			}
		}
		
		
		if(flag==false)
		{
			verifyStep(readExcel.getValue("BrouchreName")+" Brouchre name is not avaolable in UI", "FAIL");
			Assert.assertTrue(false);
		}
		verifyStep("Brouchre is Selected", "PASS");
//		WebDriverWait wait=new WebDriverWait(driver, 100);
//		wait.until(ExpectedConditions.elementToBeClickable(getLocator(prop.getProperty("selectPages"))));
		waitUntilElementClickable(prop.getProperty("selectPages"), 100);
		
		click(prop.getProperty("selectPages"));
		
		Random rand=new Random();
		getAllLinks(prop.getProperty("selectPagesDrpDwn"))
		.get(rand.nextInt(getAllLinks(prop.getProperty("selectPagesDrpDwn")).size())).click();
		verifyStep("Page is Selected", "PASS");
		
		
		if(!capData.get("Automation").equalsIgnoreCase("") && !capData.get("Automation").equalsIgnoreCase("selenium"))
		{
			click(prop.getProperty("brouchrePage"));
			getAllLinks(prop.getProperty("brouchreProdListMob"))
			.get(rand.nextInt(getAllLinks(prop.getProperty("brouchreProdListMob")).size())).click();
		}
		else
		{
			
			while(getAllLinks(prop.getProperty("brouchreProd")).size()==0)
			{
				click(prop.getProperty("selectPages"));
				
				Random rand1=new Random();
				getAllLinks(prop.getProperty("selectPagesDrpDwn"))
				.get(rand1.nextInt(getAllLinks(prop.getProperty("selectPagesDrpDwn")).size())).click();
				verifyStep("Page is Selected", "PASS");
			}
			int randomNum=rand.nextInt(getAllLinks(prop.getProperty("brouchreProd")).size());
			if(randomNum<0)
				randomNum=randomNum*-1;
			
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			WebElement elem=getAllLinks(prop.getProperty("brouchreProd"))
					.get(randomNum);
			if(capData.get("Browser").equalsIgnoreCase("firefox"))
			{
				JavascriptExecutor jse=(JavascriptExecutor) driver;
				jse.executeScript("arguments[0].scrollIntoView(true);", elem);
				jse.executeScript("arguments[0].click();", elem);
			}
			else
				elem.click();
		}
		
		verifyStep("Product is Selected", "PASS");
	}
	
	
	public String quickShop(ReadExcel readExcel)
	{
		
//		try {
//			Thread.sleep(3000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		waitUntilElementClickable(prop.getProperty("customerName"), 100);
		verifyStep("Quick View Overlay is Displayed", "PASS");
		
		try {
			Thread.sleep(8000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(getAllLinks(prop.getProperty("pdpVariantSelect")).size()>0)
		{
			click(prop.getProperty("pdpVariantSelect"));
			
			Random rand=new Random();
			getAllLinks(prop.getProperty("variantDrpDwn"))
			.get(rand.nextInt(getAllLinks(prop.getProperty("variantDrpDwn")).size())).click();
			waitUntilElementClickable(prop.getProperty("customerName"), 100);
		}
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		waitUntilElementClickable(prop.getProperty("prodQuantity"), 20);
		enterText(prop.getProperty("prodQuantity"), readExcel.getValue("ProductQuantity"));
		waitUntilElementClickable(prop.getProperty("prodQuantity"), 20);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		click(prop.getProperty("customerName"));
		
		enterText(prop.getProperty("customerName"), readExcel.getValue("CustomerName"));
		click(prop.getProperty("autoCompleteSuggestion"));
		
		String lineNum=getText(prop.getProperty("lineNumber"));
		
		verifyStep("Quick View Overlay Details Entered", "PASS");
		click(prop.getProperty("addToOrderButton"));
//		try {
//			Thread.sleep(5000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		//waitUntilElementClickable(prop.getProperty("addToOrderButton"), 100);
		WebDriverWait wait = new WebDriverWait(driver, 20); // timeout = 20 secs
		
	
		final String javaScriptToLoadAngular =
                "var injector = window.angular.element('body').injector();" + 
                "var $http = injector.get('$http');" + 
                "return ($http.pendingRequests.length === 0)";

        ExpectedCondition<Boolean> pendingHttpCallsCondition = new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {
                return ((JavascriptExecutor) driver).executeScript(javaScriptToLoadAngular).equals(true);
            }
        };
        wait.until(pendingHttpCallsCondition);
		if(verifyElement(prop.getProperty("orderHeader")))
			verifyStep("Added To Order", "PASS");
		else
			verifyStep("Adding To Order failed", "FAIL");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		click(prop.getProperty("closeBtn"));
		return lineNum;
	}
	
}
