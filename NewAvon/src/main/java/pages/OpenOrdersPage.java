package pages;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.ExtentTest;

import framework.utils.DBConn;
import framework.utils.ReadExcel;
//import framework.utils.ReadExcel;

import framework.wrappers.NewAvonWrapper;


public class OpenOrdersPage extends NewAvonWrapper {
	private  Properties prop;
	public Map<String,String> capData = new HashMap<String,String>();
	DBConn db;
	
	public OpenOrdersPage(RemoteWebDriver driver, ExtentTest test, String mrkt, Map<String,String> capData)
	{
		this.driver = driver;
		this.test = test;
		this.capData=capData;
		this.market=mrkt;
		prop = new Properties();
		
		try {
			prop.load(new FileInputStream(new File("./src/main/Resources/angular/New Avon/OpenOrdersPage.properties")));
			db= new DBConn(mrkt);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public OpenOrdersPage orderCheckout(ReadExcel readExcel, String lineNumber)
	{
		
		try {
			
//			WebDriverWait wait=new WebDriverWait(driver, 100);
			
		
						
			if(verifyElement(prop.getProperty("openOrdersHeader"))){
				
				System.out.println("My Open Orders Page");
			}
			
			/*  ...verify product added is present in open orders page...   */
			verifyStep("Open Orders Page", "PASS");
			List<WebElement>itemsAddedList =getAllLinks(prop.getProperty("itemsList"));
			System.out.println("Items list size :"+itemsAddedList.size());
			Thread.sleep(5000);
			for(WebElement element:itemsAddedList){
				
				click(prop.getProperty("expandBtn"));
				
				verifyTextOnPage(lineNumber);
				//verifyTextOnPage(readExcel.geValue("pdtId"));
								
			}
			
		
			waitUntilElementClickable(prop.getProperty("checkoutBtn"), 100);
//			wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(getLocator(prop.getProperty("checkoutBtn")))));
			
			verifyStep("Check Out Button is Enabled", "PASS");
			
			click(prop.getProperty("checkoutBtn"));
			
			/*  Add Brochure   */
			waitUntilElementClickable(prop.getProperty("withoutBrochure"), 100);
//			wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(getLocator(prop.getProperty("withoutBrochure")))));
			
			
			String brochNeeded=readExcel.getValue("BrouchreRequired");
				
			verifyStep("Brouchre Required Pop up", "PASS");
			
			if(brochNeeded.equalsIgnoreCase("No")){
				
				click(prop.getProperty("withoutBrochure"));
				
			}else {
				
				click(prop.getProperty("addBrochure"));
			}
		

		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return this;
	}
	

	
}
