package pages;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.ExtentTest;

import framework.utils.DBConn;
import framework.utils.ReadExcel;
import framework.wrappers.NewAvonWrapper;


public class LoginPage extends NewAvonWrapper {
	private  Properties prop;
	public Map<String,String> capData = new HashMap<String,String>();
	DBConn db;
	
	public LoginPage(RemoteWebDriver driver, ExtentTest test, String mrkt, Map<String,String> capData)
	{
		this.driver = driver;
		this.test = test;
		this.capData=capData;
		this.market=mrkt;
		prop = new Properties();
		
		try {
			prop.load(new FileInputStream(new File("./src/main/Resources/angular/New Avon/Login.properties")));
			db= new DBConn(mrkt);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public LoginPage login(ReadExcel readExcel)
	{
//		WebDriverWait wait=new WebDriverWait(driver, 200);
//		//System.out.println("testingggggggg");
//		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(getLocator(prop.getProperty("signIn")))));
		waitUntilElementClickable(prop.getProperty("signIn"), 200);
		enterText(prop.getProperty("accountNumber"), readExcel.getValue("AccountNumber"));
		enterText(prop.getProperty("password"), readExcel.getValue("Password"));
		verifyStep("LoginPage", "PASS");
		
		click(prop.getProperty("signIn"));
		verifyStep("Clicked on Sign In Button", "PASS");
		return this;
	}
	
}
