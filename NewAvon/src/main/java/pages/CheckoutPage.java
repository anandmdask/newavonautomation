package pages;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.ExtentTest;

import framework.utils.DBConn;
import framework.utils.ReadExcel;

import framework.wrappers.NewAvonWrapper;


public class CheckoutPage extends NewAvonWrapper {
	private  Properties prop;
	public Map<String,String> capData = new HashMap<String,String>();
	DBConn db;
	
	public CheckoutPage(RemoteWebDriver driver, ExtentTest test, String mrkt, Map<String,String> capData)
	{
		this.driver = driver;
		this.test = test;
		this.capData=capData;
		this.market=mrkt;
		prop = new Properties();
		
		try {
			prop.load(new FileInputStream(new File("./src/main/Resources/angular/New Avon/CheckoutPage.properties")));
			db= new DBConn(mrkt);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public CheckoutPage orderSubmit(ReadExcel readExcel)
	{
		
		try {
			
//			WebDriverWait wait=new WebDriverWait(driver, 100);
				
			if(verifyElement(prop.getProperty("checkoutHeader"))){
				
				System.out.println("Checkout Page");
			}
			
			//wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(getLocator(prop.getProperty("submitOrderBtn")))));
			waitUntilElementClickable(prop.getProperty("submitOrderBtn"), 100);
			Thread.sleep(15000);
				
			if(verifyElementIsDisplayed(prop.getProperty("submitOrderBtn"))){
				
				System.out.println("submit order button is enabled");
				
				while(verifyElement(prop.getProperty("overlayPage")))
				Thread.sleep(2000);
				click(prop.getProperty("submitOrderBtn"));
			
			}
			else{
				
				/*     Select Payment option if Submit Order button is disabled       */
				
				/*  ...select payment option...   */
				
				List<WebElement>paymentOptionsList =getAllLinks(prop.getProperty("paymentOptions"));
				System.out.println("Payment Option list size :"+paymentOptionsList.size());
				
				for(WebElement element:paymentOptionsList){
				
					if(element.getText().equalsIgnoreCase(readExcel.getValue("payOption"))){
					//if(element.getText().equalsIgnoreCase("Use another credit card")){	
						
						element.click();
						break;
					}
				}
				
				//wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(getLocator(prop.getProperty("continueBtn")))));
				waitUntilElementClickable(prop.getProperty("continueBtn"), 100);
				//enterText(prop.getProperty("amount"), "150");
				enterText(prop.getProperty("amount"), readExcel.getValue("Amount"));
						
				click(prop.getProperty("cardNumber"));
				//enterText(prop.getProperty("cardNumber"), "4444444444444448");
				enterText(prop.getProperty("cardNumber"), readExcel.getValue("CardNo"));
				
				//enterText(prop.getProperty("nameOnCard"), "XXXXXX XXXXX");
				enterText(prop.getProperty("nameOnCard"), readExcel.getValue("Name"));
				
				/*  ...select month...   */
				
				click(prop.getProperty("expMonth"));
				List<WebElement>monthList =getAllLinks(prop.getProperty("expMMListItem"));
				System.out.println("Month list size :"+monthList.size());
				
				for(WebElement element:monthList){
				
					if(element.getText().equalsIgnoreCase(readExcel.getValue("Month"))){
					//if(element.getText().equalsIgnoreCase("05")){	
						
						element.click();
						break;
					}
				}
				
				/*  ...select year...   */
				
				click(prop.getProperty("expYear"));
				List<WebElement>yearList =getAllLinks(prop.getProperty("expYYListItem"));
				System.out.println("Month list size :"+yearList.size());
				
				for(WebElement element:yearList){
				
					if(element.getText().equalsIgnoreCase(readExcel.getValue("Year"))){
					//if(element.getText().equalsIgnoreCase("2020")){	
						
						element.click();
						break;
					}
				}
				
				//enterText(prop.getProperty("cvv"), "636");
				enterText(prop.getProperty("cvv"), readExcel.getValue("cvv"));
				
				//click(prop.getProperty("billingAddrChkbx"));
				
				//enterText(prop.getProperty("billingAddress"), "Infosys");
				enterText(prop.getProperty("billingAddress"), readExcel.getValue("billingAddress"));
				
				//enterText(prop.getProperty("zip"), "75835");
				enterText(prop.getProperty("zip"), readExcel.getValue("zip"));
				
				//enterText(prop.getProperty("city"), "HOPEWELL");
				enterText(prop.getProperty("city"), readExcel.getValue("city"));
				
				/*  ...select state...   */
				
				click(prop.getProperty("state"));
				List<WebElement>stateList =getAllLinks(prop.getProperty("stateList"));
				System.out.println("State list size :"+stateList.size());
				
				for(WebElement element:stateList){
				
					if(element.getText().equalsIgnoreCase(readExcel.getValue("state"))){
					//if(element.getText().equalsIgnoreCase("TX")){	
						
						element.click();
						break;
					}
				}
				
				
				/*  ...select country...   */
				
				click(prop.getProperty("country"));
				List<WebElement>countryList =getAllLinks(prop.getProperty("stateList"));
				System.out.println("Country list size :"+countryList.size());
				
				for(WebElement element:countryList){
				
					if(element.getText().equalsIgnoreCase(readExcel.getValue("country"))){
					//if(element.getText().equalsIgnoreCase("United States")){	
						
						element.click();
						break;
					}
				}
				
				
				click(prop.getProperty("continueBtn"));
				
				//wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(getLocator(prop.getProperty("submitOrderBtn")))));
				waitUntilElementClickable(prop.getProperty("submitOrderBtn"), 100);
			}
			

				

		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		verifyStep("Order Submitetd Successfully", "PASS");
		return this;
	}
	

	
}
