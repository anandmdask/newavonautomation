package pages;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;


import com.relevantcodes.extentreports.ExtentTest;

import framework.utils.DBConn;
import framework.utils.ReadExcel;

import framework.wrappers.NewAvonWrapper;


public class ProductDetailsPage extends NewAvonWrapper {
	private  Properties prop;
	public Map<String,String> capData = new HashMap<String,String>();
	DBConn db;
	
	public ProductDetailsPage(RemoteWebDriver driver, ExtentTest test, String mrkt, Map<String,String> capData)
	{
		this.driver = driver;
		this.test = test;
		this.capData=capData;
		this.market=mrkt;
		prop = new Properties();
		
		try {
			prop.load(new FileInputStream(new File("./src/main/Resources/angular/New Avon/ProductDetailsPage.properties")));
			db= new DBConn(mrkt);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public String addToOrder(ReadExcel readExcel)
	{
		String pdtId = null;
		try {
			
//		WebDriverWait wait=new WebDriverWait(driver, 100);
//		
//				
//		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(getLocator(prop.getProperty("selectCampaign")))));
		waitUntilElementClickable(prop.getProperty("selectCampaign"), 100);		
		verifyStep("Product Details Page Validated", "PASS");
			
		
		
		

		/*  ...select campaign...   */
		
		click(prop.getProperty("selectCampaign"));
		List<WebElement>campaignList =getAllLinks(prop.getProperty("campaign"));
		System.out.println("Campaign list size :"+campaignList.size());
		
		for(WebElement element:campaignList){
		
			//	if(element.getText().equalsIgnoreCase(readExcel.getValue("campaign"))){
			if(element.getText().contains(readExcel.getValue("Campgain"))){	
				
				element.click();
				break;
			}
		}
		
		
		/*  ...select shade...   */
		
		if(verifyElement(prop.getProperty("shade"))){

			click(prop.getProperty("selectShade"));
			List<WebElement>shadeList =getAllLinks(prop.getProperty("shadeList"));
			System.out.println("Shade list size :"+shadeList.size());
			
			Random rand=new Random();
			shadeList.get(rand.nextInt(shadeList.size())).click();
			verifyStep("Shade Selected", "PASS");

		}
		

		
		
		
		/*  ...enter quantity...   */
		
		enterText(prop.getProperty("pdtQuantity"),readExcel.getValue("ProductQuantity"));
		
		
			
		/*  ...select customer...   */
		//wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(getLocator(prop.getProperty("custSelectOption")))));
		
		Thread.sleep(3000);
		
		click(prop.getProperty("custSelectOption"));
		List<WebElement>customerNameList =getAllLinks(prop.getProperty("customer"));
		System.out.println("Customer list size :"+customerNameList.size());
		
		for(WebElement element:customerNameList){
		
			//	if(element.getText().equalsIgnoreCase(readExcel.getValue("custName"))){
			if(element.getText().equalsIgnoreCase(readExcel.getValue("CustomerName"))){	
				
				element.click();
				break;
			}
		}
		pdtId=getText(prop.getProperty("pdtId"));
		System.out.println("Product Id : "+pdtId);
		verifyStep("Customer Name is Selectd", "PASS");
		/*  ...Add to Order...   */
		
		click(prop.getProperty("addToOrderBtn"));
		
		if(verifyElement(prop.getProperty("pdtAddedBanner")))
			verifyStep("Product Added Banner displayed", "PASS");
		else
			verifyStep("Product Added Banner is not displayed", "PASS");
		//waitUntilVisibilityOf(prop.getProperty("pdtAddedBanner"));
		
		System.out.println("Product Added Successfully banner displayed ");
	
		
		verifyStep("Validate Product Details", "PASS");
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return pdtId;
	}
	

	
	public ProductDetailsPage VerifyCartItems(){
		
		try{
			
			click(prop.getProperty("cartIcon"));
			
			waitUntilVisibilityOf(prop.getProperty("viewOrdersBtn"));
			verifyStep("Cart Selected", "PASS");
			click(prop.getProperty("viewOrdersBtn"));
			
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return this;
	}
	
	
}
